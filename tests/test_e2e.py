import pytest
import requests
import os
import uuid

from final_project.models import Note, Notes


@pytest.fixture
def server_socket():
    return os.environ['SERVER_SOCKET']


def test_server_up(server_socket: str):
    res = requests.get(f"http://{server_socket}/health")

    assert res.status_code == 200, res.text


def test_readwrite(server_socket: str):
    note = Note(text=str(uuid.uuid4()))
    res = requests.post(f"http://{server_socket}/notes", json=note.dict())

    assert res.status_code == 200, res.text

    res = requests.get(f"http://{server_socket}/notes")

    assert res.status_code == 200, res.text
    notes = Notes.parse_obj(res.json())

    assert note in notes.__root__
