## Run tests

1. Setup Database
2. Setup server and provide envs (example in docker-compose.yml)
3. Provide server socket address to tests (check test file)
4. Run tests:
```bash
python -m pytest tests
```
