from pydantic import BaseModel


class Note(BaseModel):
    text: str


class Notes(BaseModel):
    __root__: list[Note]


